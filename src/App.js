import React, { Component } from "react";
import Box from "./components/Box";
import Buttons from "./components/Buttons";

class App extends Component {
  state = {
    count: 0
  };
  //Step 1 Create a function
  handleAdd = () => {
    this.setState({
      count: this.state.count + 1
    });
  };

  handleMinus = () => {
    this.setState({
      count: this.state.count - 1
    });
  };

  handleReset = () => {
    this.setState({
      count: 0
    });
  };

  handleMultiply = () => {
    this.setState({
      count: this.state.count * 2
    });
  };

  render() {
    return (
      // A React Objects
      // Step 2 send to Buttons
      <React.Fragment>
        <Box count={this.state.count} />
        <Buttons
          handleAdd={this.handleAdd}
          handleMinus={this.handleMinus}
          handleReset={this.handleReset}
          handleMultiply={this.handleMultiply}
        />
      </React.Fragment>
    );
  }
}

export default App;
