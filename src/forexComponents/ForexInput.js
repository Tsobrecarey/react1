// Functional Base:
import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

function ForexInput(props) {
  // render is classbased
  // functional holds return
  return (
    <FormGroup>
      <Label>{props.label}</Label>
      <Input
        // we will remove this inside the object, since classbased
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        onChange={props.onChange}
        type="number"
      />
    </FormGroup>
  );
}

export default ForexInput;

// Class Base:
// import React, { Component } from "react";
// import { FormGroup, Label, Input } from "reactstrap";

// class ForexInput extends Component {
//   render() {
//     return (
//       <FormGroup>
//         <Label>{this.props.label}</Label>
//         <Input
//           placeholder={this.props.placeholder}
//           defaultValue={this.props.defaultValue}
//           onChange={this.props.onChange}
//           type="number"
//         />
//       </FormGroup>
//     );
//   }
// }

// export default ForexInput;
