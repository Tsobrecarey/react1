import React, { useEffect, useState } from "react";
import Cruds from "./crudComponents/Cruds";
import { Card, CardHeader, CardBody, Table, Button } from "reactstrap";
import CrudForm from "./crudComponents/CrudForm";

const CRUDApp = () => {
  const [questions, setQuestions] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [questionToEdit, setQuestionToEdit] = useState({});
  const [questionIndex, setQuestionIndex] = useState(null);

  useEffect(() => {
    fetch(
      "https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=boolean"
    )
      .then(res => res.json())
      .then(res => {
        setQuestions(res.results);
      });
  }, []);

  const deleteQuestion = index => {
    const newQuestion = questions.filter((question, qIndex) => {
      return qIndex !== index;
    });

    setQuestions(newQuestion);
  };

  const saveQuestion = (question, category) => {
    let newQuestions = [];
    if (isEditing) {
      let editedQuestion = question;
      let editedCategory = category;

      if (question === "") {
        editedQuestion = questionToEdit.question;
      }
      if (category === "") {
        editedCategory = questionToEdit.category;
      }

      newQuestions = questions.map((indivQuestion, index) => {
        if (index === questionIndex) {
          return { question: editedQuestion, category: editedCategory };
        } else {
          return indivQuestion;
        }
      });
    } else {
      newQuestions = [...questions, { question: question, category: category }];
    }
    setIsEditing(false);
    setQuestionToEdit({});
    setQuestionIndex(null);
    setQuestions(newQuestions);
    setShowForm(false);
  };

  const editQuestion = (question, index) => {
    setShowForm(true);
    setIsEditing(true);
    setQuestionToEdit(question);
    setQuestionIndex(index);
  };

  const toggleForm = () => {
    setShowForm(false);
    setIsEditing(false);
    setQuestionToEdit({});
  };

  return (
    <div className="col-lg-8 offset-lg-2">
      <Card className="my-5">
        <CardHeader className="bg-secondary">
          <h1 className="text-center text-info">Trivia Questions</h1>
        </CardHeader>
        <CardHeader>
          <Button color="info" onClick={() => setShowForm(!showForm)}>
            + Add Question
          </Button>
          <CrudForm
            showForm={showForm}
            toggleForm={toggleForm}
            saveQuestion={saveQuestion}
            isEditing={isEditing}
            questionToEdit={questionToEdit}
          />
        </CardHeader>
        <CardBody>
          {/* Questions here */}
          <Table>
            <thead>
              <tr>
                <td>Questions</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <Cruds
                questions={questions}
                deleteQuestion={deleteQuestion}
                toggleForm={editQuestion}
              />
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </div>
  );
};

export default CRUDApp;
