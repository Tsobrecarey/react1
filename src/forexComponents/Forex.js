//Funtional Base:
import React, { useState } from "react";
import ForexDropdown from "./ForexDropdown";
import ForexInput from "./ForexInput";
import { Button } from "reactstrap";
import ForexRates from "./ForexRates";

const Forex = () => {
  const [amount, setAmount] = useState(0);
  const [baseCurrency, setBaseCurrency] = useState(null);
  const [targetCurrency, setTargetCurrency] = useState(null);
  const [convertedAmount, setConvertedAmount] = useState(0);
  const [showTable, setShowTable] = useState(false);
  const [rates, setRates] = useState([]);

  const handleAmount = e => {
    setAmount(e.target.value);
  };

  const handleBaseCurrency = currency => {
    const code = currency.code;
    fetch("https://api.exchangeratesapi.io/latest?base=" + code)
      .then(res => res.json())
      .then(res => {
        const ratesArray = Object.entries(res.rates);
        setRates(ratesArray);
        setShowTable(true);
      });

    setBaseCurrency(currency);
  };

  const handleTargetCurrency = currency => {
    setTargetCurrency(currency);
  };

  const handleConvert = () => {
    const code = baseCurrency.code;
    fetch("https://api.exchangeratesapi.io/latest?base=" + code)
      .then(res => res.json())
      .then(res => {
        const targetCode = targetCurrency.code;
        const rate = res.rates[targetCode];
        setConvertedAmount(amount * rate);
      });
  };

  return (
    <div style={{ width: "70%" }}>
      <h1 className="text-center my-5">Forex Calculator</h1>
      {showTable === true ? (
        <div>
          <h2 className="text-center">
            Exchange Rate for:{baseCurrency.currency}
          </h2>
          <div className="d-flex justift-content-center">
            <ForexRates rates={rates} />
          </div>
        </div>
      ) : (
        ""
      )}
      <div
        className="d-flex justify-content-around"
        style={{ margin: "0 200px" }}
      >
        <ForexDropdown
          label={"Base Currency"}
          onClick={handleBaseCurrency}
          currency={baseCurrency}
        />
        <ForexDropdown
          label={"Target Currency"}
          onClick={handleTargetCurrency}
          currency={targetCurrency}
        />
      </div>
      <div className="d-flex justify-content-around">
        <ForexInput
          label={"Amount"}
          placeholder={"Amount to convert"}
          onChange={handleAmount}
        />
        <Button color="info" onClick={handleConvert}>
          Convert
        </Button>
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <h1 className="text-center mx-2">{convertedAmount}</h1>
      </div>
    </div>
  );
};

export default Forex;

//Class Base:
// import React, { Component } from "react";
// import ForexDropdown from "./ForexDropdown";
// import ForexInput from "./ForexInput";
// import { Button } from "reactstrap";
// import ForexRates from "./ForexRates";

// class Forex extends Component {
//   state = {
//     amount: 0,
//     baseCurrency: null,
//     targetCurrency: null,
//     convertedAmount: 0,
//     code: " "
//   };

//   handleAmount = e => {
//     this.setState({
//       amount: e.target.value
//     });
//   };

//   handleBaseCurrency = currency => {
//     this.setState({
//       baseCurrency: currency
//     });
//   };

//   handleTargetCurrency = currency => {
//     this.setState({
//       targetCurrency: currency
//     });
//   };

//   handleConvert = () => {
//     if (
//       this.state.baseCurrency == null &&
//       this.state.targetCurrency == null &&
//       this.state.amount <= 0
//     ) {
//       this.setState({ convertedAmount: "Enter All Details!" });
//     } else {
//       const code = this.state.baseCurrency.code;
//       fetch("https://api.exchangeratesapi.io/latest?base=" + code)
//         .then(res => res.json())
//         .then(res => {
//           const targetCode = this.state.targetCurrency.code;
//           const rate = res.rates[targetCode];
//           this.setState({ convertedAmount: this.state.amount * rate });
//           this.setState({ code: this.state.targetCurrency.code });
//         });
//     }
//   };

//   render() {
//     return (
//       <div style={{ width: "70%" }}>
//         <h1 className="text-center my-5">Forex Calculator</h1>
//         <div
//           className="d-flex justify-content-around"
//           style={{ margin: "0 200px" }}
//         >
//           <ForexDropdown
//             label={"Base Currency"}
//             onClick={this.handleBaseCurrency}
//             currency={this.state.baseCurrency}
//           />
//           <ForexDropdown
//             label={"Target Currency"}
//             onClick={this.handleTargetCurrency}
//             currency={this.state.targetCurrency}
//           />
//         </div>
//         <div className="d-flex justify-content-around">
//           <ForexInput
//             label={"Amount"}
//             placeholder={"Amount to convert"}
//             onChange={this.handleAmount}
//           />
//           <Button color="info" onClick={this.handleConvert}>
//             Convert
//           </Button>
//         </div>
//         <div className="d-flex justify-content-center align-items-center">
//           <h1 className="text-center mx-2">{this.state.convertedAmount}</h1>
//           <h1 className="text-center mx-2">{this.state.code}</h1>
//         </div>
//         <div className="d-flex justify-content-around flex-column my-5">
//           <h1 className="text-center">Forex Rates</h1>
//           <ForexRates />
//         </div>
//       </div>
//     );
//   }
// }

// export default Forex;
