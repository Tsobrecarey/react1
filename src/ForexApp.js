//Functional Base:
import React from "react";
import Forex from "./forexComponents/Forex";

// below is also the other method of functional based, and usually practiced..
// function ForexApp(){} ||
const ForexApp = () => {
  return (
    <div className="bg-secondary d-flex justify-content-center align-items-center min-vh-100">
      <Forex />
    </div>
  );
};

export default ForexApp;

//Class Base:
// import React, { Component } from "react";
// import Forex from "./forexComponents/Forex";

// class ForexApp extends Component {
//   render() {
//     return (
//       <div className="bg-secondary d-flex justify-content-center align-items-center vh-100">
//         <Forex />
//       </div>
//     );
//   }
// }

// export default ForexApp;
