import React, { Component } from "react";

class Button extends Component {
  //props
  render() {
    return (
      // Step 4. Use the function we received as property in our event listener
      <button className={this.props.color} onClick={this.props.handleOnClick}>
        {this.props.text}
      </button>
    );
  }
}

export default Button;
