const Currencies = [
  {
    currency: "Philippine Peso",
    code: "PHP"
  },
  {
    currency: "US Dollar",
    code: "USD"
  },
  {
    currency: "European",
    code: "EUR"
  },
  {
    currency: "Japanese Yen",
    code: "JPY"
  },
  {
    currency: "British Pound",
    code: "GBP"
  },
  {
    currency: "South Korean Won",
    code: "KRW"
  },
  {
    currency: "Thai Baht",
    code: "THB"
  },
  {
    currency: "Indonesian Ringgit",
    code: "INR"
  }
];

export default Currencies;
