//Functional Base:
import React, { useState } from "react";
import {
  FormGroup,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import Currencies from "./ForexData";

function ForexDropdown(props) {
  //useState is a React hook
  const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
  const [currency, setCurrency] = useState(null);

  return (
    <FormGroup>
      <Label>{props.label}</Label>
      <Dropdown
        isOpen={dropdownIsOpen}
        toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
      >
        <DropdownToggle caret>
          {!props.currency ? "Choose Currency" : props.currency.currency}
        </DropdownToggle>
        <DropdownMenu>
          {Currencies.map((currency, index) => (
            <DropdownItem key={index} onClick={() => props.onClick(currency)}>
              {currency.currency}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
}

export default ForexDropdown;

//CLass Base:
// import React, { Component } from "react";
// import {
//   FormGroup,
//   Label,
//   Dropdown,
//   DropdownToggle,
//   DropdownMenu,
//   DropdownItem
// } from "reactstrap";
// import Currencies from "./ForexData";

// class ForexDropdown extends Component {
//   state = {
//     dropdownIsOpen: false,
//     currency: null
//   };

//   render() {
//     return (
//       <FormGroup>
//         <Label>{this.props.label}</Label>
//         <Dropdown
//           isOpen={this.state.dropdownIsOpen}
//           toggle={() =>
//             this.setState({ dropdownIsOpen: !this.state.dropdownIsOpen })
//           }
//         >
//           <DropdownToggle caret>
//             {!this.props.currency
//               ? "Choose Currency"
//               : this.props.currency.currency}
//           </DropdownToggle>
//           <DropdownMenu>
//             {Currencies.map((currency, index) => (
//               <DropdownItem
//                 key={index}
//                 onClick={() => this.props.onClick(currency)}
//               >
//                 {currency.currency}
//               </DropdownItem>
//             ))}
//           </DropdownMenu>
//         </Dropdown>
//       </FormGroup>
//     );
//   }
// }

// export default ForexDropdown;
