import React from "react";
import { Button } from "reactstrap";

const Cruds = props => {
  return (
    <React.Fragment>
      {props.questions.map((question, index) => (
        <tr key={index}>
          <td>
            <p>Question #{index + 1}</p>
            <p className="text-danger">{question.question}</p>
            <p className="text-secondary">{question.category}</p>
          </td>
          <td>
            <Button
              style={{ width: "75px" }}
              color="danger"
              onClick={() => props.deleteQuestion(index)}
              className="my-1"
            >
              Delete
            </Button>
            <Button
              style={{ width: "75px" }}
              color="info"
              onClick={() => props.toggleForm(question, index)}
              className="my-1"
            >
              Edit
            </Button>
          </td>
        </tr>
      ))}
    </React.Fragment>
  );
};

export default Cruds;
