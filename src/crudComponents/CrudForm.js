import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Button
} from "reactstrap";

const CrudForm = props => {
  const [question, setQuestion] = useState("");
  const [category, setCategory] = useState("");

  const handleSaveQuestion = () => {
    props.saveQuestion(question, category);
    setQuestion("");
    setCategory("");
  };

  return (
    <Modal isOpen={props.showForm} toggle={props.toggleForm}>
      <ModalHeader toggle={props.toggleForm}>
        {props.isEditing ? "Edit Question" : "Add Question"}
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label>Question:</Label>
          <Input
            placeholder="Type your question"
            onChange={e => setQuestion(e.target.value)}
            defaultValue={props.questionToEdit.question}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label>Category:</Label>
          <Input
            placeholder="Type the Category"
            onChange={e => setCategory(e.target.value)}
            defaultValue={props.questionToEdit.category}
          ></Input>
        </FormGroup>
        <Button color="success" onClick={handleSaveQuestion}>
          {props.isEditing ? "Edit Question" : "Add Question"}
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default CrudForm;
